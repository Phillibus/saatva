## Mattress Shop Page
This single page React Application was created using create-react-app.

It uses Redux to maintain the currently selected mattress, whose image is showing on the page; 
the selected mattress that will go into the cart, and all the available mattresses that were pulled from the 
json file.

## Installation
use git clone https://gitlab.com/Phillibus/saatva.git  (using https)

install Node modules with npm install.
