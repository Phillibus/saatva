import {combineReducers} from "redux";
import MattressReducer from "./mattress-reducer";

export const rootReducer = combineReducers({
    Mattresses: MattressReducer
});