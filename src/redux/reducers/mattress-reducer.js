import {ADD_TO_CART, MAIN_ACTION, SELECT_BUTTON} from "../actions/main-action";

const initState = {
    mattressData: [],
    selectedMattress: {},
    cartItems: []
};

const MattressReducer = (state = initState, action) => {
    switch (action.type) {
        case MAIN_ACTION:
            return ({
                ...state,
                mattressData: action.payload
            });
        case SELECT_BUTTON:
            return ({
                ...state,
                selectedMattress: action.payload
            });
        case ADD_TO_CART:
            return ({
               ...state,
                cartItems: [...state.cartItems, action.payload]
            });
        default:
            return state;
    }
};

export default MattressReducer;