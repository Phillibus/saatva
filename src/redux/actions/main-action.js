import {DATA} from "../../data/mattresses";

export const MAIN_ACTION = "MAIN_ACTION";
export const SELECT_BUTTON = 'SELECT_BUTTON';
export const ADD_TO_CART = "ADD_TO_CART";

export const onLoad = () => {
    return dispatch => {
        const dataArray = Object.keys(DATA.mattresses).map(field => DATA.mattresses[field]);
        Promise.resolve(
            dispatch({
                type: MAIN_ACTION,
                payload: dataArray
            }))
            .then(dispatch({
                type: SELECT_BUTTON,
                payload: dataArray[0]
            }));
    }
}