export const removeSpacesAndSpecialCharacters = str => str ? str.replace(/[^a-zA-Z0-9]/g, "") : '';

export const formatAsDollars = str => str ? str.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,') : '';

export const imageURLBuilder = (path) => (imageName) => `${path}${imageName}`;