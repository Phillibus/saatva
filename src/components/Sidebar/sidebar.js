import React, {memo, useEffect} from 'react';
import {useSelector, useDispatch} from "react-redux";
import {removeSpacesAndSpecialCharacters, formatAsDollars} from "../../utils/utils";
import {SELECT_BUTTON, ADD_TO_CART} from "../../redux/actions/main-action";

const Sidebar = () => {
    const dispatch = useDispatch();

    // Redux Store
    const mattresses = useSelector(state => state.Mattresses.mattressData);
    const currentMattress = useSelector(state => state.Mattresses.selectedMattress);

    // Utility Functions
    const getPrice = price => price ? `$${formatAsDollars(price)}` : '$0.00';

    // Events
    const handleButtonClick = event => {
        event.preventDefault();

        const selectedLabel = event.target.innerText;
        const selectedMattressObject = mattresses.find(mattress => mattress.name === selectedLabel);
        dispatch({
            type: SELECT_BUTTON,
            payload: selectedMattressObject
        });
    };

    const handleAddToCart = event => {
        event.preventDefault();

        if (Object.keys(currentMattress).length > 0) {
            dispatch({
                type: ADD_TO_CART,
                payload: currentMattress
            })
        }
    };

    useEffect(() => {
        const buttonArray = Array.from(document.querySelectorAll('.select-btn'));
        const selectedButton = buttonArray.find(button => button.innerText === currentMattress.name);

        if (selectedButton) {
            selectedButton.classList.add('selected');
        }

    }, [currentMattress]);

    return (
        <div id="side-bar" role='region'>
            <h1
                className="choose-your-mattress-header"
            >
                Choose Your Mattress
            </h1>
            <div className="button-group">
                <h3 className="button-group-header content-text-font-family">
                    SELECT MATTRESS TYPE
                </h3>
                {
                    mattresses.map((mattress) => (
                        <button
                            type="button"
                            className={`select-btn btn-${removeSpacesAndSpecialCharacters(mattress.name)}`}
                            key={mattress.name}
                            onClick={handleButtonClick}
                            aria-label={`${mattress.name} button that displays this mattress' info and image.`}
                        >
                            <span className="content-text-font-family">{mattress.name}</span>
                        </button>
                    ))
                }
            </div>
            <div className="review">
                <div className="review-title"><span className="content-text-font-family">{currentMattress.name}</span></div>
                <div className="review-price">
                    <span className="content-text-font-family">
                        {getPrice(currentMattress.price)}
                    </span>
                </div>
            </div>
            <button
                type="button"
                className="add-to-cart-button"
                onClick={handleAddToCart}
                aria-label={`Add ${currentMattress.name} to your cart.`}
            >
                <span className="content-text-font-family">Add to Cart</span>
            </button>
        </div>
    )
}

export default memo(Sidebar);