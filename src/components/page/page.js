import React from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {onLoad} from "../../redux/actions/main-action";
import Carousel from "../carousel/carousel";
import Sidebar from "../Sidebar/sidebar";
import '../../App.css';

class Page extends React.Component {
    componentDidMount() {
        this.props.init();
    }

    render() {
        return (
            <div id='page' role='main'>
                <div className="top-bar">
                    <h2 className="top-bar-header">saatva</h2>
                    <div className='cart'>Items in Carts
                        {' '}
                        {
                            this.props.cartItems.length > 0 ? this.props.cartItems.length : ''
                        }
                    </div>
                </div>
                <div className="main-content">
                    <Carousel/>
                    <Sidebar/>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    cartItems: state.Mattresses.cartItems,
    mainMattressData: state.Mattresses.mattressData
});

const mapDispatchToProps = dispatch => ({
    init: () => dispatch(onLoad())
});

Page.propTypes = {
    currentMattress: PropTypes.array,
    init: PropTypes.func.isRequired,
    mainMattressData: PropTypes.array.isRequired
};

export default connect(mapStateToProps, mapDispatchToProps)(Page);
