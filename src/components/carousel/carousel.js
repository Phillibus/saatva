import React from 'react';
import {useSelector} from "react-redux";
import {imageURLBuilder, removeSpacesAndSpecialCharacters} from "../../utils/utils";

const Carousel = () => {
    // Redux State
    const currentMattress = useSelector(state => state.Mattresses.selectedMattress);

    // Utility Functions
    const getImageUrl = imageURLBuilder('images/');

    return (
        <div
            id='carousel'
            role="complementary"
            aria-label='An image carousel that displays an image of the currently selected mattress.'
        >
            <img
                className={`banner-image ${removeSpacesAndSpecialCharacters(currentMattress.name)}-img`}
                key={currentMattress.name}
                src={getImageUrl(currentMattress.imageFileName)}
                alt={currentMattress.name}
                role="presentation"
            />
        </div>
    );
};

export default Carousel;
